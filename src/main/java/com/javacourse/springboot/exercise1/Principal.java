package com.javacourse.springboot.exercise1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.javacourse.springboot.exercise1.operations.Operation;

import lombok.Cleanup;

public class Principal {

	static Logger logger = LoggerFactory.getLogger(Principal.class);

	public static void main(String[] args) {

		@Cleanup
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

		// scan explícito
		context.scan("com.javacourse.springboot.exercise1.operations");
		context.refresh();

		Operation operation = (Operation) context.getBean(args[0]);
		int result = operation.execute(Integer.valueOf(args[1]), Integer.valueOf(args[2]));

		System.out.println(args[0] + " " + args[1] + " " + args[2] + " = " + String.valueOf(result));

		logger.info("Info message");
	}

}
