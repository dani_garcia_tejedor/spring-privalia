package com.javacourse.springboot.exercise1.operations;

public interface Operation {
	
	int execute(int op1, int op2);

}
