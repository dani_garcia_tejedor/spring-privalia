package com.javacourse.springboot.exercise1.operations;

import org.springframework.stereotype.Component;

@Component(value="sum")
public class Sum implements Operation {

	@Override
	public int execute(int op1, int op2) {
		return op1 + op2;
	}

}
