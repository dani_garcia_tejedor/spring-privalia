package com.privalia.aspects.xml;

import org.aspectj.lang.JoinPoint;

public class Validator {
	
	void checkQuantity() {
		System.out.println("Checking quantity...");
	}

	void pack() {
		System.out.println("Packing order...");
	}
	
	void callHelpDesk(JoinPoint jp,Throwable error) {
		System.out.println("Calling helpdesk...");
        System.out.println("Method Signature: "  + jp.getSignature());  
        System.out.println("Exception is: "+error);  
	}


}
