package com.privalia.aspects.xml;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {

	public static void main(String[] args) throws Exception {

		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");

		Compra compra = (Compra) context.getBean("compra");
		
		System.out.println("COMPRA OK");
		compra.comprar(false);
		
		try {
			System.out.println("COMPRA ERROR");
			compra.comprar(true);
		} catch(Exception e) {
			
		}
	}

}
