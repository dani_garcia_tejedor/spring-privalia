package com.privalia.aspects.annotations;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {

	public static void main(String[] args) throws Exception {

		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");

		Compra compra = (Compra) context.getBean("compra2");
		
		System.out.println("COMPRA OK");
		compra.comprar(false);
		
		try {
			System.out.println("COMPRA ERROR");
			compra.comprar(true);
		} catch(Exception e) {
			
		}
		System.out.println("FIN");
	}

}
