package com.privalia.aspects.annotations;

public class CompraImpl implements Compra {
	
	public void comprar(boolean $error) throws Exception {
		if($error) {
			throw new Exception("Error happened in CompraImpl...");
		}
		System.out.println("Comprando...");
	}

}
