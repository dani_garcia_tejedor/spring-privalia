package com.privalia.aspects.annotations;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Aspect
@EnableAspectJAutoProxy
public class Validator {
	
	@Before("execution( * com.privalia.aspects.annotations.Compra.comprar(..) )")
	void checkQuantity() {
		System.out.println("Checking quantity...");
	}
	
	@AfterReturning("execution( * com.privalia.aspects.annotations.Compra.comprar(..) )")
	void pack() {
		System.out.println("Packing order...");
	}
	
	@AfterThrowing("execution( * com.privalia.aspects.annotations.Compra.comprar(..) )")
	void callHelpDesk(JoinPoint jp,Throwable error) {
		System.out.println("Calling helpdesk...");
        System.out.println("Method Signature: "  + jp.getSignature());  
        System.out.println("Exception is: "+error);  
	}


}
