package com.privalia.entity;

import lombok.Data;

@Data
public class Student {

	int idstudent;

	String name;

	String surname;

	int age;

	Address address;

	/**
	 * @param idStudent
	 * @param name
	 * @param surname
	 * @param age
	 * @param address
	 */
	public Student(int idStudent, String name, String surname, int age, Address address) {
		super();
		this.idstudent = idStudent;
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.address = address;
	}

	/**
	 * 
	 */
	public Student() {
		super();
	}

}
