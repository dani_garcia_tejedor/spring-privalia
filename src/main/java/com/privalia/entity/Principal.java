package com.privalia.entity;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import lombok.Cleanup;

public class Principal {

	public static void main(String[] args) {
		
		@Cleanup
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
		
		Student student = (Student) context.getBean("student");
		System.out.println(student);
		
		Student studentCons = (Student) context.getBean("studentConstructor");		
		System.out.println(studentCons);
		

	}

}
