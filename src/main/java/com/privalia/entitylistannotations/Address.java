package com.privalia.entitylistannotations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Address {
	
	@Value("Werwer")
	String street;
	
	@Value("123123")
	int number;
	
	@Value("Cangas de Onís")
	String city;

}
