package com.privalia.entitylistannotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import lombok.Cleanup;

public class Principal {

	public static void main(String[] args) {
		
		@Cleanup
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.scan("com.privalia.entitylistannotations");
		context.refresh();
		
		Student student = (Student) context.getBean(Student.class);		
		System.out.println(student);
	}

}
