package com.privalia.entitylistannotations;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Student {

	int idstudent;

	String name;

	String surname;

	int age;

	@Autowired
	Address address;
	
	@Resource(name="getTeacherList")
	List<Teacher> listTeacher;

}
