package com.privalia.entitylistannotations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ListConfiguration {
	
	List<Teacher> teacherList;
	
	@Bean
	List<Teacher> getTeacherList() {
		 teacherList = new ArrayList<Teacher>();
		 teacherList.add(new Teacher(7, "Pepe"));
		 teacherList.add(new Teacher(55, "Bart"));
		 return teacherList;
	}

}
