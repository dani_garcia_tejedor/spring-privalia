package com.privalia.entitylistannotations;

import org.springframework.beans.factory.annotation.Value;

import lombok.Data;

@Data
public class Teacher {

	@Value("17")
	int idTeacher;
	@Value("Jordi")
	String name;
	
	/**
	 * @param idTeacher
	 * @param name
	 */
	public Teacher(int idTeacher, String name) {
		super();
		this.idTeacher = idTeacher;
		this.name = name;
	}
	
	
}
