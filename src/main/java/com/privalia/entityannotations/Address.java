package com.privalia.entityannotations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Address {
	
	@Value("Pez")
	String street;
	
	@Value("99")
	int number;
	
	@Value("Bujaraloz")
	String city;

}
