package com.privalia.entityannotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@PropertySource("classpath:config.properties")
public class Student {

	@Value("${student.idstudent}")
	int idstudent;

	@Value("${student.name}")
	String name;

	@Value("${student.surname}")
	String surname;

	@Value("${student.age}")
	int age;

	@Autowired
	Address address;

}
