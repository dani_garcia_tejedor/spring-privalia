package com.privalia.builderexample;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import lombok.Cleanup;

public class Principal {

	public static void main(String[] args) {
		@Cleanup
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
		Employee employee = (Employee) context.getBean("employee");
		System.out.println(employee.toString());
	}
}


//public class Principal {
//
//	public static void main(String[] args) {
//
////		Employee employee = new Employee.Builder().name("Pepiño").build();
////		System.out.println(employee.toString());
//		
//		
//	}

//}
