package com.privalia.builderexample;

public class Employee {
	private int id;
	private String name;
	private String department;
	private String address;
	private String phone;
	private String mobile;

	private Employee(String name, String deptt, String address, String phone, String mobile) {
		super();
		this.id = generateId();
		this.name = name;
		this.department = deptt;
		this.address = address;
		this.phone = phone;
		this.mobile = mobile;
	}

	private int generateId() {
		// Generate an id with some mechanism
		int id = 0;
		return id;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", department=" + department + ", address=" + address
				+ ", phone=" + phone + ", mobile=" + mobile + "]";
	}

	static public class Builder {
		private int id;
		private String name;
		private String department;
		private String address;
		private String phone;
		private String mobile;

		public Builder() {
		}
		
		public Builder(int id, String name) {
			this.id = id;
			this.name = name;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder department(String deptt) {
			this.department = deptt;
			return this;
		}

		public Builder address(String address) {
			this.address = address;
			return this;
		}

		public Employee build() {
			Employee emp = new Employee(name, department, address, phone, mobile);
			return emp;
		}
	}
}