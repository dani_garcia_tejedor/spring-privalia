package com.privalia.external.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {

	private final static Logger logger = LoggerFactory.getLogger(Principal.class);

	public static void main(String[] args) {

		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");

		Database database = (Database) context.getBean("database");

		String message = String.format("Url: %s, Username: %s, Password: %s", database.getUrl(), database.getUsername(),
				database.getPassword());
		
		logger.info(message);

	}

}
