package com.privalia.entitylist;

import lombok.Data;

@Data
public class Address {
	
	String street;
	
	int number;
	
	String city;

	/**
	 * @param street
	 * @param number
	 * @param city
	 */
	public Address(String street, int number, String city) {
		super();
		this.street = street;
		this.number = number;
		this.city = city;
	}
}
