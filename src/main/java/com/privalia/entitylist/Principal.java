package com.privalia.entitylist;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import lombok.Cleanup;

public class Principal {

	public static void main(String[] args) {
		
		@Cleanup
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
		
		Student studentConsList = (Student) context.getBean("studentConstList");		
		System.out.println(studentConsList);
	}

}
