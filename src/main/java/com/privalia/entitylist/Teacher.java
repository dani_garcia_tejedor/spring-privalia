package com.privalia.entitylist;

import lombok.Data;

@Data
public class Teacher {

	int idTeacher;
	
	String name;
}
