package com.privalia.entitylist;

import java.util.List;

import lombok.Data;

@Data
public class Student {

	int idstudent;

	String name;

	String surname;

	int age;

	Address address;
	
	List<Teacher> listTeacher;

	/**
	 * 
	 */
	public Student() {
		super();
	}



	/**
	 * @param idstudent
	 * @param name
	 * @param surname
	 * @param age
	 * @param address
	 * @param listTeacher
	 */
	public Student(int idstudent, String name, String surname, int age, Address address, List<Teacher> listTeacher) {
		super();
		this.idstudent = idstudent;
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.address = address;
		this.listTeacher = listTeacher;
	}

}
