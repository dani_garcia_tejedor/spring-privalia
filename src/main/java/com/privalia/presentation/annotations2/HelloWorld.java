package com.privalia.presentation.annotations2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component(value="helloWorld")
public class HelloWorld {

	private String hello;

	public HelloWorld(String hello) {
		super();
		this.hello = hello;
	}

	public HelloWorld() {
		super();
	}
	
	public String getHello() {
		return hello;
	}

	// DI en setter
	@Autowired
	@Value("Hello from annotations in setter!")
	public void setHello(String hello) {
		this.hello = hello;
	}
	
}
