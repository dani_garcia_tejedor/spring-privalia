package com.privalia.presentation.annotations3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.privalia.presentation.annotations3.HelloWorld;

import lombok.Cleanup;

public class Principal {

	public static void main(String[] args) {
		@Cleanup
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		
		// scan explícito
		context.scan("com.privalia.presentation.annotations3");
		context.refresh();

		// no utilizo el id del bean, sino la clase
		HelloWorld helloWorld = (HelloWorld) context.getBean(HelloWorld.class);
		System.out.println(helloWorld.getHello());
	}

}
