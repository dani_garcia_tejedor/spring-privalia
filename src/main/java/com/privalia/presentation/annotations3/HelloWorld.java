package com.privalia.presentation.annotations3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component(value="helloWorld")
public class HelloWorld {

	private String hello;

	// DI en constructor, value en param
	@Autowired
	public HelloWorld(@Value("Hello from annotations in constructor!") String hello) {
		super();
		this.hello = hello;
	}

	public HelloWorld() {
		super();
	}
	
	public String getHello() {
		return hello;
	}

	public void setHello(String hello) {
		this.hello = hello;
	}
	
}
