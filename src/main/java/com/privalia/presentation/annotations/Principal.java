package com.privalia.presentation.annotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import lombok.Cleanup;

public class Principal {

	public static void main(String[] args) {
		@Cleanup
		AbstractApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);

		HelloWorld helloWorld = (HelloWorld) context.getBean("helloWorld");
		System.out.println(helloWorld.getHello());
	}

}
